#coding=utf8
from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw
import sys
import pandas as pd

# month -> x = 260, y = 560, size = 30
# half_write_place_horizontal = 300
# size_write_place_vertical = 250
# y_max = month_y + size_write_place_vertical = 560 + 250
# size_name = half_write_place_horizontal / longest_name = 300 / len
# name -> x = 80, y = manth_y + month_size + size_name / 2 = 560 + 30 +  (300 / len) / 2
# date -> x = 80 + half_write_place_horizontal(320), y = name_y

coord_EOS = [280, 560, 30, 120, 600, 25, 220, 540, 300, 250, 320]
coord_TT = [280, 390, 30, 120, 600, 25, 220, 540, 300, 250, 280]


def get_new_jpg(names, dates, month, project, count):
    coord = coord_EOS if project == "EOS" else coord_TT

    if len(month) == 2:
        month = "{} and {}".format(month[0], month[1])
        month_zero = coord[0] / 2
    if len(month) == 1:
        month = month[0]
        month_zero = coord[0]
    print("Project ->", project)
    path = "pdf_in/{}.jpg".format(project)

    longest_name = 0
    for name in names:
        if longest_name < len(name):
            longest_name = len(name)
    size = int(coord[8] / longest_name) - 2

    if len(month) > 8:
        month_zero -= 50
    y_max = coord[1] + coord[9]
    n = 0
    while 1:
        img = Image.open(path)
        draw = ImageDraw.Draw(img)

        font_month = ImageFont.truetype("../..//Downloads/SignPainter_HouseBrush.ttf", coord[2])
        draw.text((month_zero, coord[1]), month, (225, 102, 51), font=font_month)

        check = True
        y = coord[1] + coord[2] + int(size / 2)
        size -= (n)
        font = ImageFont.truetype("../..//Downloads/SignPainter_HouseBrush.ttf", size + 10)
        for name, date in zip(names, dates):
            draw.text((coord[3], y), name, (0, 0, 123), font=font)
            draw.text((coord[3] + coord[10] - 30, y), date, (0, 0, 123), font=font)
            y += size + (size - n - 3)
            if y > y_max:
                n += 2 #TODO Change this )))
                check = False
                break
        if check is True:
            img.save("pdf_out/{}_{}.jpg".format(project, month))
            img.show()
            break


def get_names(file):
    df = pd.read_excel(file)
    names = df["Name"].tolist()
    date = df["Date"].tolist()
    return names, date


def check_params(list_files):
    if len(list_files) == 2:
        if "xlsx" in list_files[0] and ("EOS" in list_files[1] or "TT" in list_files[1] or "MC" in list_files[1]):
            pass
        else:
            hendle_print()
    if len(list_files) == 3:
        if "xlsx" in list_files[0] and "xlsx" in list_files[1] and ("EOS" in list_files[2] or "TT" in list_files[2]):
            pass
        else:
            hendle_print()


def hendle_print():
    print("You must gave me 2 args:\n"
          " -> 1. xlsx file\n"
          " -> 2. Name of project\n"
          "Or...if you need yoy can pass 3 args:\n"
          " -> 1. xlsx file\n"
          " -> 2. xlsx file\n"
          " -> 3. Name of project\n")
    exit(0)


def main(list_files):
    month = []
    check_params(list_files)
    if len(list_files) > 1:
        if len(list_files) == 2:
            file1 = list_files[0]
            file2 = None
            project = list_files[1]
            month.append(file1.split("_")[1].split(".")[0])
            names, date = get_names(file1)
        if len(list_files) == 3:
            file1 = list_files[0]
            file2 = list_files[1]
            project = list_files[2]
            month.append(file1.split("_")[1].split(".")[0])
            month.append(file2.split("_")[1].split(".")[0])
            names1, date1 = get_names(file1)
            names2, date2 = get_names(file1)
            names = names1 + names2
            date = date1 + date2
        get_new_jpg(names, date, month, project, len(names))
        print("SUCCESS!!!")
    else:
        hendle_print()


if __name__ == '__main__':
    main(sys.argv[1:])
